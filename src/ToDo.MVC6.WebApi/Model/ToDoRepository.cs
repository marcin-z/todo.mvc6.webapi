﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace ToDo.MVC6.WebApi.Model
{
    public class ToDoRepository : IToDoRepository
    {
        static ConcurrentDictionary<string, ToDoItem> _listToDo = new ConcurrentDictionary<string, ToDoItem>();

        public ToDoRepository()
        {
            Init();    
        }

        private void Init()
        {
            Add(new ToDoItem { Name = "Task 1", DateLimit = DateTime.Now.AddDays(3) });
            Add(new ToDoItem { Name = "Task 2", DateLimit = DateTime.Now.AddDays(3) });
            Add(new ToDoItem { Name = "Task 3", DateLimit = DateTime.Now.AddDays(3) });
        }

        public IEnumerable<ToDoItem> GetAll()
        {
            return _listToDo.Values;
        }

        public string Add(ToDoItem item)
        {
            item.Key = Guid.NewGuid().ToString();
            _listToDo[item.Key] = item;
            return item.Key.ToString();
        }

        public ToDoItem Find(string key)
        {
            ToDoItem item;
            _listToDo.TryGetValue(key, out item);
            return item;
        }

        public ToDoItem Remove(string key)
        {
            ToDoItem item;
            _listToDo.TryGetValue(key, out item);
            _listToDo.TryRemove(key, out item);
            return item;
        }

        public void Completed(ToDoItem item)
        {
            item.IsComlete = true;
            item.DateCompleted = DateTime.Now;
        }
    }
}
