﻿using System.Collections.Generic;
using Microsoft.AspNet.Mvc;
using ToDo.MVC6.WebApi.Model;

namespace SimpleApi.Controllers
{
    [Route("api/[controller]")]
    public class ToDoController : Controller
    {
        [FromServices]
        public IToDoRepository ToDoRepo { get; set; }

        [HttpGet]
        public IEnumerable<ToDoItem> GetAll()
        {
            return ToDoRepo.GetAll();
        }

        public IActionResult GetById(string id)
        {
            var todoItem = ToDoRepo.Find(id);
            if (todoItem == null)
            {
                return HttpNotFound();
            }
            return new ObjectResult(todoItem);
        }

        [HttpPost]
        public IActionResult Create([FromBody] ToDoItem item)
        {
            if (item == null)
            {
                return HttpBadRequest();
            }
            ToDoRepo.Add(item);
            return CreatedAtRoute("GetTodo", new { controller = "Todo", id = item.Key }, item);
        }

        [HttpPut("{id}")]
        public IActionResult Completed(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                return HttpBadRequest();
            }

            var todo = ToDoRepo.Find(id);
            if (todo == null)
            {
                return HttpNotFound();
            }
            ToDoRepo.Completed(todo);
            return new NoContentResult();
        }


        [HttpDelete("{id}")]
        public IActionResult Delete(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                return HttpBadRequest();
            }

            var todo = ToDoRepo.Find(id);
            if (todo == null)
            {
                return HttpNotFound();
            }

            ToDoRepo.Remove(id);

            return new NoContentResult();
        }

    }
}